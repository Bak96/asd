#include <iostream>
#include <stdio.h>

using namespace std;

void wypiszTablice(long *tab, long n) {
    for (long i = 0; i < n; i++) {
        printf("%ld, ", tab[i]);
    }
    printf("\n");
}

void wczytajCene(long *cena, long n) {
    int x;
    for (long i = 0; i < n; i++) {
        scanf("%ld", &cena[i]);
    }
}

void policzSumy(long long *suma, long *cena, long n) {
    suma[n-1] = cena[n-1];
    for (long i = n-2; i >= 0; i--) {
        suma[i] = suma[i+1] + cena[i];
    }
}

void policzParzystePrawa(long *parzystePrawa, long *cena, long n) {
    if (cena[n-1] % 2 == 0) {
        parzystePrawa[n-1] = cena[n-1];
    }
    else {
        parzystePrawa[n-1] = -1;
    }

    for (int i = n - 2; i >= 0; i--) {
        if (cena[i] % 2 == 0) {
            parzystePrawa[i] = cena[i];
        }
        else {
            parzystePrawa[i] = parzystePrawa[i+1];
        }
    }
}

void policzParzysteLewa(long *parzysteLewa, long *cena, long n) {
    parzysteLewa[0] = -1;

    for (long i = 1; i < n; i++) {
        if (cena[i-1] % 2 == 0) {
            parzysteLewa[i] = cena[i-1];
        }
        else {
            parzysteLewa[i] = parzysteLewa[i-1];
        }
    }
}

void policzNieparzystePrawa(long *nieparzystePrawa, long *cena, long n) {
    if (cena[n-1] % 2 != 0) {
        nieparzystePrawa[n-1] = cena[n-1];
    }
    else {
        nieparzystePrawa[n-1] = -1;
    }

    for (int i = n - 2; i >= 0; i--) {
        if (cena[i] % 2 != 0) {
            nieparzystePrawa[i] = cena[i];
        }
        else {
            nieparzystePrawa[i] = nieparzystePrawa[i+1];
        }
    }
}

void policzNieparzysteLewa(long *nieparzysteLewa, long *cena, long n) {
    nieparzysteLewa[0] = -1;

    for (long i = 1; i < n; i++) {
        if (cena[i-1] % 2 != 0) {
            nieparzysteLewa[i] = cena[i-1];
        }
        else {
            nieparzysteLewa[i] = nieparzysteLewa[i-1];
        }
    }
}

void wypiszInfo(long *cena, long *suma, long *parzystePrawa, long *parzysteLewa,
                long *nieparzystePrawa, long *nieparzysteLewa, long n) {

    printf("Ceny\n");
    wypiszTablice(cena, n);

    printf("sumy\n");
    wypiszTablice(suma, n);

    printf("parzystePrawa\n");
    wypiszTablice(parzystePrawa, n);

    printf("parzysteLewa\n");
    wypiszTablice(parzysteLewa, n);

    printf("nieparzystePrawa\n");
    wypiszTablice(nieparzystePrawa, n);

    printf("nieparzysteLewa\n");
    wypiszTablice(nieparzysteLewa, n);

}

void wypisz(long long n) {
    printf("%lld\n", n);
}

int main()
{
    long n;
    scanf("%ld", &n);

    long *cena = new long [n];
    long long *suma = new long long [n];
    long *parzystePrawa = new long [n];
    long *parzysteLewa = new long [n];
    long *nieparzystePrawa = new long [n];
    long *nieparzysteLewa = new long [n];

    wczytajCene(cena, n);
    policzSumy(suma, cena, n);
    policzParzystePrawa(parzystePrawa, cena, n);
    policzParzysteLewa(parzysteLewa, cena, n);
    policzNieparzystePrawa(nieparzystePrawa, cena, n);
    policzNieparzysteLewa(nieparzysteLewa, cena, n);

    //wypiszInfo(cena, suma, parzystePrawa, parzysteLewa, nieparzystePrawa, nieparzysteLewa, n);

    long m;
    scanf("%ld", &m);

    //printf("---\n");

    for (long i = 0; i < m; i++) {
        long k; //liczba produktow
        scanf("%ld", &k);

        //suma najdrozszych produktow
        if(suma[n - k] % 2 != 0) {
            wypisz(suma[n - k]);
            continue;
        }

        bool zamiana1 = true;
        bool zamiana2 = true;

        if (parzystePrawa[n - k] == -1 || nieparzysteLewa[n - k] == -1) {
            zamiana1 = false;
        }
        if (nieparzystePrawa[n - k] == -1 || parzysteLewa[n - k] == -1) {
            zamiana2 = false;
        }

        long long suma1 = suma[n - k] - parzystePrawa[n - k] + nieparzysteLewa[n - k];
        long long suma2 = suma[n - k] - nieparzystePrawa[n - k] + parzysteLewa[n - k];
        if (zamiana1 && zamiana2) {
            if(suma1 > suma2) {
                wypisz(suma1);
            }
            else {
                wypisz(suma2);
            }
        }
        else if (zamiana1) {
            wypisz(suma1);
        }
        else if (zamiana2) {
            wypisz(suma2);
        }
        else {
            wypisz(-1);
        }
    }

    delete [] cena;
    delete [] suma;
    delete [] parzystePrawa;
    delete [] parzysteLewa;
    delete [] nieparzystePrawa;
    delete [] nieparzysteLewa;
}
