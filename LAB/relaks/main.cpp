#include <iostream>
#include <stdio.h>
#include <vector>
using namespace std;

int get(vector<vector<vector<vector<int>>>> &MAX, int wierszWchodzenia, int kolumnaWchodzenia, int kolumnaSchodzenia, int poziom) {
	if (wierszWchodzenia < 0 || wierszWchodzenia >= MAX.size() ||
		kolumnaSchodzenia < 0 || kolumnaSchodzenia >= MAX[0].size() ||
		kolumnaWchodzenia < 0 || kolumnaWchodzenia >= MAX[0].size()) {
		return 0;
	}
	else {
		return MAX[wierszWchodzenia][kolumnaWchodzenia][kolumnaSchodzenia][poziom];
	}
}

void obliczPoziomZero(vector<vector<vector<vector<int>>>> &MAX, vector<vector<int>> &zbocze, int wierszWchodzenia, int kolumnaWchodzenia, int kolumnaSchodzenia) {
	int max = 0;
	
	for (int i = kolumnaWchodzenia - 2; i <= kolumnaWchodzenia + 2; i++) {
		for (int j = kolumnaSchodzenia - 1; j <= kolumnaSchodzenia + 1; j++) {
			for (int poziom = 1; poziom <= 2; poziom++) {
				
				int punkty = get(MAX, wierszWchodzenia - 1, i, j, poziom);
				if (punkty > max) {
					max = punkty;
				}
			}
		}
	}
	
	MAX[wierszWchodzenia][kolumnaWchodzenia][kolumnaSchodzenia][0] = max + zbocze[wierszWchodzenia][kolumnaWchodzenia] + zbocze[wierszWchodzenia][kolumnaSchodzenia];
}

void obliczResztePoziomow(vector<vector<vector<vector<int>>>> &MAX, vector<vector<int>> &zbocze, int wierszWchodzenia, int kolumnaWchodzenia, int kolumnaSchodzenia) {
	
	for (int liczonyPoziom = 1; liczonyPoziom <= 2; liczonyPoziom++) {
		int max = 0;
		for (int i = kolumnaWchodzenia - 2; i <= kolumnaWchodzenia + 2; i++) {
			if (i == kolumnaSchodzenia && liczonyPoziom == 1) continue; //ok????
			
			int punkty = get(MAX, wierszWchodzenia - 1, i, kolumnaSchodzenia, liczonyPoziom - 1);
			
			if (punkty > max) {
				max = punkty;
			}
		}
		MAX[wierszWchodzenia][kolumnaWchodzenia][kolumnaSchodzenia][liczonyPoziom] = max + zbocze[wierszWchodzenia][kolumnaWchodzenia];
	}
}

int obliczWierszMaxa(vector<vector<vector<vector<int>>>> &MAX, vector<vector<int>> &zbocze, int nrWiersza) {
	int max = 0;
	for (int kolumnaWchodzenia = 0; kolumnaWchodzenia < MAX[0].size(); kolumnaWchodzenia++) {
		for (int kolumnaSchodzenia = 0; kolumnaSchodzenia < MAX[0].size(); kolumnaSchodzenia++) {
			if (kolumnaWchodzenia == kolumnaSchodzenia) {
				obliczResztePoziomow(MAX, zbocze, nrWiersza, kolumnaWchodzenia, kolumnaSchodzenia);
				continue;
			}
			
			obliczPoziomZero(MAX, zbocze, nrWiersza, kolumnaWchodzenia, kolumnaSchodzenia);
			obliczResztePoziomow(MAX, zbocze, nrWiersza, kolumnaWchodzenia, kolumnaSchodzenia);
			
			int wybrano = get(MAX, nrWiersza, kolumnaWchodzenia, kolumnaSchodzenia, 0);
			if (wybrano > max) {
				max = wybrano;
			}
		}
	}
	
	return max;
}

int main()
{
	int m, n;
	scanf("%d %d", &m, &n);

	vector<vector<int>> zbocze;
	zbocze.resize(m);
	for (int i = 0; i < m; i++) {
		zbocze[i].resize(n);
	}

	for (int i = 0; i < m; i++) {
		for (int j = 0; j < n; j++) {
			scanf("%d", &zbocze[i][j]);
		}
	}
	
	vector<vector<vector<vector<int>>>> MAX;
	MAX.resize(m); 
	
	for (int i = 0; i < m; i++) {
		MAX[i].resize(n);
		for (int j = 0; j < n; j++) {
			MAX[i][j].resize(n);
			for (int k = 0; k < n; k++) {
				MAX[i][j][k].resize(3);
			}
		}
	}

	int max = 0;
	
	for (int nrWiersza = 0; nrWiersza < m; nrWiersza++) {
		int maxZWiersza = obliczWierszMaxa(MAX, zbocze, nrWiersza);
		if (maxZWiersza > max) {
			max = maxZWiersza;
		}
	}
	
	printf("%d", max);
}
