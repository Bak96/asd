#include <iostream>
#include <string>
#include <stdio.h>

using namespace std;

int main()
{
    string line;
    getline(cin, line);

    long shortestDistance = line.size();
    long d1 = -1;

    size_t i;

    for (i = 0; i < line.size(); i++) {
        if (line.at(i) != '*') {
            d1 = i;
            break;
        }

    }

    for (i = d1 + 1; d1 >= 0 && i < line.size(); i++) {
        if (line.at(i) == line.at(d1)) {
            d1 = i;
            continue;
        }

        if (line.at(i) != line.at(d1) && line.at(i) != '*') {
            long distance = i - d1;

            if (distance < shortestDistance) {
                shortestDistance = distance;
            }

            d1 = i;
        }
    }

    long w;
    if (d1 < 0) {
        w = 1;
    }
    else {
        w = line.size() - shortestDistance + 1;
    }

    printf("%ld\n", w);

    return 0;
}
