#include <iostream>

using namespace std;

void drawTable(long **p, int n) {
    cout << "********" << endl;
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            cout << p[i][j] << ", ";
        }
        cout << endl;
    }

    cout << "********" << endl;
}

void cleanTable(long **p, int n) {
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            p[i][j] = 0;
        }
    }
}

void initPossibilities(long **p, int *numbers, int n) {
    for (int i = 0; i < n - 1; i++) {
        //comparing numbers[ax, ax+1]
        if (numbers[i] <= numbers[i+1]) {
            p[i][i+1] = 1;
            p[i+1][i] = 1;
        }
        else {
            p[i][i+1] = 0;
            p[i+1][i] = 0;
        }
    }
}

void generatePossibilities(long **p, int *numbers, int n) {
    //podciagi dlugosci k
    for (int k = 3; k <= n; k++) {
        for (int i = 0; i <= n - k; i++) {
            int CL1 = 0;
            int CR1 = 0;
            int CL2 = 0;
            int CR2 = 0;

            if (numbers[i+1] > numbers[i]) {
                CL1 = p[i+1][i+k-1];
            }
            if (numbers[i+k-1] > numbers[i]) {
                CR1 = p[i+k-1][i+1];
            }
            if (numbers[i+k-1] > numbers[i]) {
                CL2 = p[i][i+k-2];
            }
            if (numbers[i+k-1] > numbers[i+k-2]) {
                CR2 = p[i+k-2][i];
            }

            p[i][i+k-1] = (CL1 + CR1) % 1000000000;
            p[i+k-1][i] = (CL2 + CR2) % 1000000000;
        }
       // drawTable(p, n);
    }
}

int main()
{
    int n;
    scanf("%d", &n);

    if (n == 1) {
        printf("%d\n", 1);
        return 0;
    }

    long** possibilities = new long*[n];
    for(int i = 0; i < n; ++i) {
        possibilities[i] = new long[n];
    }

    //drawTable(possibilities, n);

    int* numbers = new int[n];

    for (int i = 0; i < n; i++) {
        scanf("%d", &numbers[i]);
    }

    cleanTable(possibilities, n);
    //drawTable(possibilities, n);
    initPossibilities(possibilities, numbers, n);
    //drawTable(possibilities, n);
    generatePossibilities(possibilities, numbers, n);
    printf("%ld\n", (possibilities[0][n-1] + possibilities[n-1][0]) % 1000000000);

    return 0;
}


