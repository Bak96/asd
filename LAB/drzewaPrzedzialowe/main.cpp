#include <vector>
#include <stdio.h>
#include <cmath>

using namespace std;

const int MILIARD = 1000000000;

void wypiszInwersje(vector<int> &w) {
	int N = w.size() / 2;
	for (int j = 0; j < N; j++) {
		printf("%d, ", w[N + j]);
	}
	printf("|,");
	for (int j = 1; j < N; j++) {
		printf("%d, ", w[j]);
	}

	printf("\n");
}
void wypiszPoziomyInwersji(vector<vector<int>> &poziomyInwersji) {
	printf("Listuje Inwersje ---------------\n");
	for (int i = 0; i < poziomyInwersji.size(); i++) {
		printf("inwersja %d: ", i);
		wypiszInwersje(poziomyInwersji[i]);
	}
	printf("Koniec Listowania --------------\n");

}

void wstaw(vector<int> &dp, int val, int poz) {
	int v = dp.size() / 2 + poz;
	dp[v] = val % MILIARD;

	while (v != 1) {
		v /= 2;
		dp[v] = dp[2 * v] + dp[2 * v + 1];
		dp[v] = dp[v] % MILIARD;
	}
}

int liczSume(vector<int> &dp, int a, int b) {
	if (b < a) {
		return 0;
	}

	int va = dp.size() / 2 + a;
	int vb = dp.size() / 2 + b;

	int wyn = dp[va];

	if (va != vb) wyn += dp[vb];

	wyn = wyn % MILIARD;

	while (va / 2 != vb / 2) {
		if (va % 2 == 0) wyn += dp[va + 1];
		wyn = wyn % MILIARD;
		if (vb % 2 == 1) wyn += dp[vb - 1];

		wyn = wyn % MILIARD;
		va /= 2;
		vb /= 2;
	}

	return wyn;
}

void aktualizujInwersje(vector<vector<int>> &poziomyInwersji, int num) {
	int N = poziomyInwersji[0].size() / 2;

	wstaw(poziomyInwersji[0], 1, num - 1);

	for (size_t i = 1; i < poziomyInwersji.size(); i++) {
        int sum = liczSume(poziomyInwersji[i - 1], num, N - 1);
        /*optymalizacja*/
		if (i < poziomyInwersji.size() - 1 &&
				sum == poziomyInwersji[i][N + num - 1]) {
			break;
		}

		wstaw(poziomyInwersji[i], sum, num - 1);
	}
}

int main()
{
	int n, k;
	scanf("%d %d", &n, &k);

	vector<vector<int>> poziomyInwersji;
	poziomyInwersji.resize(k);

	int wielkoscDrzewaPrzedzialowego = pow(2, ceil(log2(n)) + 1);

	for (auto i = poziomyInwersji.begin(); i < poziomyInwersji.end(); i++) {
		i->resize(wielkoscDrzewaPrzedzialowego);
	}

	//wypiszPoziomyInwersji(poziomyInwersji);
	for (int i = 0; i < n; i++) {
		int num;
		scanf("%d", &num);
		aktualizujInwersje(poziomyInwersji, num);
		//wypiszPoziomyInwersji(poziomyInwersji);
    }

    //wypiszPoziomyInwersji(poziomyInwersji);
    int wynik = liczSume(poziomyInwersji[k - 1], 0, n - 1);

	printf("%d\n", wynik);
	return 0;
}
