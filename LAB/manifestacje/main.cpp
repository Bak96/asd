#include <stdio.h>
#include <vector>
#include <iostream>
#include <algorithm>
#include <queue>
bool debug = false;
class CompareDemonstrations {
public:
	bool operator()(std::pair<int, int> const &p1, std::pair<int, int> const& p2) {
		if (p1.second == p2.second) {
			return p1.first > p2.first;
		}
		else {
			return p1.second < p2.second;
		}
	}
};

void printPairs(std::vector<std::pair<int, int>> &pairs) {
	for(auto it = pairs.begin(); it != pairs.end(); ++it) {
		int time = it->first;
		int manifest = it->second;
		std::cout << time << " " << manifest << std::endl;
	}
}

void printSingle(std::vector<int> &tab) {
	for (auto it = tab.begin(); it != tab.end(); ++it) {
		std::cout << *it << std::endl;
	}
}

int main()
{
	int numOfCitizens; //1 - 1 000 000
	int numOfDemonstrations; // 1 - 100 000
	scanf("%d %d", &numOfCitizens, &numOfDemonstrations);

	//pair <czas, manifestacja>
	std::vector<std::pair<int, int>> starts(numOfCitizens); // 1 000 000
	std::vector<std::pair<int, int>> ends(numOfCitizens); // 1 000 000
	std::vector<int> queries(numOfDemonstrations); //100 000
	std::vector<int> queriesStandardOrder(numOfDemonstrations); //100 000
	//pair <manifestacja, iloscLudzi>
	std::vector<std::pair<int, int>> queriesResults(1000000); //1 000 000
	std::vector<int> actualState(100000); // 100 000
	std::priority_queue<std::pair<int,int>,std::vector<std::pair<int,int>>,CompareDemonstrations> kopiec;

	//wczytanie info o uczestnikach
	int startTime;
	int endTime;
	int demonstrationId;
	for (int i = 0; i < numOfCitizens; i++) {
		scanf("%d %d %d", &startTime, &endTime, &demonstrationId);
		starts[i] = std::pair<int, int>(startTime, demonstrationId);
        ends[i] = std::pair<int, int>(endTime, demonstrationId);
	}

	//wczytanie info o godzinach krecenia
	int time;
	for (int i = 0; i < numOfDemonstrations; i++) {
		scanf("%d", &time);
		queries[i] = time;
		queriesStandardOrder[i] = time;
	}

	//ustawienie stanu poczatkowego
	for (int i = 0; i < 100000; i++) {
        actualState[i] = 0;
		kopiec.push(std::pair<int, int>(i+1, 0));
	}

	std::sort(starts.begin(), starts.end());
	std::sort(ends.begin(), ends.end());
	std::sort(queries.begin(), queries.end());

    int lastTime = queries.back();
    int startP = 0;
    int endP = 0;
	int queryP = 0; // czy ja na pewno przechodze po tym co chce?
    for (int t = 1; t <= lastTime; t++) {
		while (startP < starts.size() && starts[startP].first == t) {
			int num = starts[startP].second; //wyciagam numer manifestacji
			actualState[num - 1]++;
			kopiec.push(std::pair<int, int>(num, actualState[num - 1]));
			startP++;
		}

		if (queries[queryP] == t) {
			std::pair<int, int> bestDemonstration = kopiec.top();

			while (bestDemonstration.second != actualState[bestDemonstration.first - 1]) {
                kopiec.pop();
                bestDemonstration = kopiec.top();
			}

			queriesResults[t-1] = bestDemonstration;
			queryP++;
		}

		while (endP < ends.size() && ends[endP].first == t) {
			int num = ends[endP].second;
			actualState[num - 1]--;
			kopiec.push(std::pair<int, int>(num, actualState[num - 1]));
			endP++;
		}
    }

    for (auto it = queriesStandardOrder.begin(); it != queriesStandardOrder.end(); ++it) {
		printf("%d %d\n", queriesResults[*it - 1].first, queriesResults[*it - 1].second);
    }

    return 0;
}
