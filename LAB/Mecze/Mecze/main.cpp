#include <iostream>
#include <stdio.h>
#include <cstdlib>
using namespace std;

/* qsort int comparison function */
int int_cmp(const void *a, const void *b)
{
    const long long *ia = (const long long *)a; // casting pointer types
    const long long *ib = (const long long *)b;

    if (*ib > *ia) {
        return -1;
    }
    else if (*ib < *ia) {
        return 1;
    }
    else {
        return 0;
    }
}

void printTable(long long *t, int n) {
    for (int i = 0; i < n; i++) {
        printf("%d, ", t[i] );
    }
    printf("\n");
}

int main(int argc, char *argv[])
{
    int n, m;
    scanf("%d %d", &n, &m);

    long long *players = new long long[n];
    for (int i = 0; i < n; i++) {
        players[i] = 0;
    }

    long long mask = 1;

    int numberOfPeopleInOneTeam = n/2;

    for (int gameNumber = 0; gameNumber < m; gameNumber++) {

        for (int i = 0; i < numberOfPeopleInOneTeam; i++) {
            int player;
            scanf("%d", &player);
            players[player - 1] = players[player - 1] | mask;
        }

        for (int i = 0; i < numberOfPeopleInOneTeam; i++) {
            int player;
            scanf("%d", &player);
        }

        mask = mask << 1;
    }
    qsort(players, n, sizeof(long long), int_cmp);

    for (int i = 1; i < n; i++) {
        if (players[i] == players[i - 1]) {
            printf("NIE\n");
            return 0;
        }
    }

    printf("TAK\n");
    return 0;
}
